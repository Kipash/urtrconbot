﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace UrTRconBot
{
    public class Config
    {
        public DiscordConfig DiscordConfig;

        public void SanetizeInstance()
        {
            if (DiscordConfig == null)
                DiscordConfig = new DiscordConfig();

            if (DiscordConfig.CommandPrefix == null)
                DiscordConfig.CommandPrefix = "!";
            if (DiscordConfig.Token == null)
                DiscordConfig.Token = "0";
        }

        const string ConfigPath = "appsettings.json";
        static JsonSerializerSettings settings = new JsonSerializerSettings()
        {
            Formatting = Formatting.Indented,
            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
            DefaultValueHandling = DefaultValueHandling.Populate,
            //NullValueHandling = NullValueHandling.Ignore,
            //MissingMemberHandling = MissingMemberHandling.Ignore,
            //ObjectCreationHandling = ObjectCreationHandling.Replace,
        };

        static Config _instance;
        public static Config LoadConfig()
        {
            if (File.Exists(ConfigPath))
            {
                var json = File.ReadAllText(ConfigPath);
                _instance = JsonConvert.DeserializeObject<Config>(json, settings);
            }
            else
            {
                _instance = new Config();
                _instance.SanetizeInstance();

                SaveConfig();

                Log.Write($"Config generated ({ConfigPath}), please fill in Token and other needed configuration!!!", LogCol.Green);
                Environment.Exit(0);
            }
            return _instance;
        }

        public static void SaveConfig()
        {
            File.Delete(ConfigPath);
            var json = JsonConvert.SerializeObject(_instance, settings);
            File.WriteAllText(ConfigPath, json);
        }
    }


    public class DiscordConfig
    {
        public string Token;
        public string CommandPrefix;
    }
}
