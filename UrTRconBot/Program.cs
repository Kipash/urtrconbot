﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
//using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
//using Newtonsoft.Json;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace UrTRconBot
{
    public class Program
    {
        DiscordSocketClient client;
        CommandService commands;
        IServiceProvider services;
        Config config;
        static void Main(string[] args)
        {
            new Program()
                .RunBotAsync()
                .GetAwaiter()
                .GetResult();
        }


        public async Task RunBotAsync()
        {
            client = new DiscordSocketClient();
            commands = new CommandService();

            config = Config.LoadConfig();

            //var rooster = new MovieRooster();
            //var api = new IMDBApi(cfg);

            services = new ServiceCollection()
                .AddSingleton(client)
                .AddSingleton(commands)
                .AddSingleton(config)
                //.AddSingleton(rooster)
                //.AddSingleton(api)
                .BuildServiceProvider();

            var token = config.DiscordConfig.Token;

            client.Log += ClientLog;
            await RegisterCommandsAsync();
            await client.LoginAsync(TokenType.Bot, token);
            await client.StartAsync();

            //await MessageManager.ServerViewLoop(client, config);
            await Task.Delay(-1);
        }

        Task ClientLog(LogMessage arg)
        {
            Console.WriteLine(arg);
            return Task.CompletedTask;
        }

        public async Task RegisterCommandsAsync()
        {
            client.MessageReceived += HandleCommandAsync;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly(), services);
        }
        async Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            var context = new SocketCommandContext(client, message);
            if (message.Author.IsBot) return;

            var prefix = config.DiscordConfig.CommandPrefix;

            int argPos = 0;
            if (message.HasStringPrefix(prefix, ref argPos))
            {
                var result = await commands.ExecuteAsync(context, argPos, services);
                if (!result.IsSuccess) Console.WriteLine(result.ErrorReason);
            }
        }
    }


    public class Log
    {
        public static void Write(string msg, LogCol col)
        {
            var currCol = Console.ForegroundColor;
            Console.ForegroundColor = (ConsoleColor)(int)col;
            Console.WriteLine(msg);
            Console.ForegroundColor = currCol;
        }
    }

    public enum LogCol
    {
        Black = 0,
        DarkBlue = 1,
        DarkGreen = 2,
        DarkCyan = 3,
        DarkRed = 4,
        DarkMagenta = 5,
        DarkYellow = 6,
        Gray = 7,
        DarkGray = 8,
        Blue = 9,
        Green = 10,
        Cyan = 11,
        Red = 12,
        Magenta = 13,
        Yellow = 14,
        White = 15
    }
}
